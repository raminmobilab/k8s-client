FROM openjdk:8-jdk-alpine
ARG JAR_FILE
ADD target/${JAR_FILE} app.jar
EXPOSE 4567

RUN adduser -D -u 1000 -g 1000 appuser && \
    chown -R 1000:1000 ${JAVA_HOME} && \
    chown -R 1000:1000 /app.jar && \
    mkdir /files && \
    chown -R 1000:1000 /files

USER appuser

ENTRYPOINT ["java",                                        \
            "-XX:+UnlockExperimentalVMOptions",            \
            "-XX:+UseCGroupMemoryLimitForHeap",            \
            "-Djava.security.egd=file:/dev/./urandom",     \
            "-cp",                                         \
            "/app.jar",                                    \
            "com.daimler.mpay.App"                                     \
]