package com.daimler.mpay;

import com.google.common.io.ByteStreams;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.Exec;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodList;
import io.kubernetes.client.models.V1Volume;
import io.kubernetes.client.models.V1VolumeMount;
import io.kubernetes.client.util.Config;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static spark.Spark.get;

public class App {
    static long t = 0;
    static Map<String, Long[]> u = new HashMap<String, Long[]>();

    public static void main(String[] args) throws Exception {
        ApiClient client = Config.defaultClient();
        Configuration.setDefaultApiClient(client);

        CoreV1Api api = new CoreV1Api();
        V1PodList list = api.listPodForAllNamespaces(null, null, null, null, null, null, null, null, null);

        Thread thread = new Thread(){
            public void run() {

                while (true) {
                    Map<String, Long[]> used = null;
                    long totalKBs = 0;
                    try {
                        used = new HashMap<String, Long[]>();
                        totalKBs = 0;
                        for (V1Pod pod : list.getItems()) {
                            for (V1Volume volume : pod.getSpec().getVolumes()) {
                                if (volume.getPersistentVolumeClaim() != null && !used.containsKey(volume.getPersistentVolumeClaim().getClaimName())) {
                                    String claimName = volume.getPersistentVolumeClaim().getClaimName();
                                    String volumeName = volume.getName();
                                    for (V1VolumeMount mount : pod.getSpec().getContainers().get(0).getVolumeMounts()) {
                                        if (mount.getName().equals(volumeName)) {
                                            System.out.println(">>>>>> " + pod.getMetadata().getName() + "###" + mount.getMountPath() + "###" + claimName);

                                            final ByteArrayOutputStream os;
                                            try {
                                                Exec exec = new Exec();
                                                final Process proc =
                                                        exec.exec(
                                                                pod.getMetadata().getNamespace(),
                                                                pod.getMetadata().getName(),
                                                                new String[] {"df"},
                                                                false,
                                                                false);

                                                os = new ByteArrayOutputStream();
                                                Thread out =
                                                        new Thread(
                                                                () -> {
                                                                    try {
                                                                        ByteStreams.copy(proc.getInputStream(), os);
                                                                    } catch (IOException ex) {
                                                                        ex.printStackTrace();
                                                                    }
                                                                });
                                                out.start();
                                                proc.waitFor();
                                                out.join();
                                                proc.destroy();


                                                String df = os.toString();
        //                            System.out.println(df);

                                                Scanner scanner = new Scanner(df);
                                                while (scanner.hasNextLine()) {
                                                    String line = scanner.nextLine();
                                                    if (line.contains(mount.getMountPath())) {
        //                                    System.out.println("line = " + line);
                                                        String[] tokens = line.split("\\s+");
        //                                    System.out.println("tokens = " + tokens);
                                                        if (tokens[5].equals(mount.getMountPath())) {
        //                                        System.out.println("###USAGE: " + tokens[4]);
                                                            long allKBs = Long.parseLong(tokens[1]);
                                                            long usedKBs = Long.parseLong(tokens[2]);
                                                            long availableKBs = Long.parseLong(tokens[3]);
                                                            long usedPercentage = Long.parseLong(tokens[4].replaceFirst(".$",""));
                                                            used.put(claimName, new Long[]{allKBs, usedKBs, availableKBs, usedPercentage});
                                                            totalKBs += usedKBs;
                                                            break;
                                                        }
                                                    }
                                                }
                                                scanner.close();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }
                                }
                            }
                        }

                        t = totalKBs;
                        u = new HashMap<>(used);
                        System.out.println("totalKBs = " + totalKBs);
                        for (String claim : used.keySet()) {
                            System.out.println(claim + ": " + Arrays.toString(used.get(claim)));
                        }

                        Thread.sleep(60000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        };
        thread.start();

        get("/metrics", (req, res) -> getMetrics());

    }

    private static String getMetrics() {
//        System.out.println("getMetrics..." + u.size());
        StringBuilder sb = new StringBuilder();
        sb.append("total_pvc_usage_kb ").append(t).append("\n");
        for (String key: u.keySet()) {
            Long[] values = u.get(key);
            sb = sb.append("pvc_all_kb").append("{pvc=\"").append(key).append("\"").append("} ").append(values[0]).append("\n");
            sb = sb.append("pvc_used_kb").append("{pvc=\"").append(key).append("\"").append("} ").append(values[1]).append("\n");
            sb = sb.append("pvc_available_kb").append("{pvc=\"").append(key).append("\"").append("} ").append(values[2]).append("\n");
            sb = sb.append("pvc_used_percentage").append("{pvc=\"").append(key).append("\"").append("} ").append(values[3]).append("\n");
        }
        String s = sb.toString();
//        System.out.println(s);
        return s;
    }
}